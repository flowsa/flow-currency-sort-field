<?php
/**
 * Currency Sort field plugin for Craft CMS 3.x
 *
 * Normalise currencies
 *
 * @link      https://www.flowsa.com
 * @copyright Copyright (c) 2019 Flow Comunications
 */

namespace flowsa\currencysortfield;


use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\console\Application as ConsoleApplication;

use yii\base\Event;

/**
 * Class CurrencySortField
 *
 * @author    Flow Comunications
 * @package   CurrencySortField
 * @since     1.0.0
 *
 */
class CurrencySortField extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var CurrencySortField
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $schemaVersion = '1.0.0';

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        if (Craft::$app instanceof ConsoleApplication) {
            $this->controllerNamespace = 'flowsa\currencysortfield\console\controllers';
        }

        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                }
            }
        );

        Craft::info(
            Craft::t(
                'currency-sort-field',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================

}
