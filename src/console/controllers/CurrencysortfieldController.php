<?php
/**
 * Currency Sort field plugin for Craft CMS 3.x
 *
 * Normalise currencies
 *
 * @link      https://www.flowsa.com
 * @copyright Copyright (c) 2019 Flow Comunications
 */

namespace flowsa\currencysortfield\console\controllers;

use flowsa\currencysortfield\CurrencySortField;
use flowsa\money\Money;
use Craft;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Currencysortfield Command
 *
 * @author    Flow Comunications
 * @package   CurrencySortField
 * @since     1.0.0
 */
class CurrencysortfieldController extends Controller
{
    // Public Methods
    // =========================================================================

    /**
     * Handle currency-sort-field/currencysortfield console commands
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $result = 'something';

        $entries = \craft\elements\Entry::find()
            ->sectionId([15,17])
            ->all();

        foreach ($entries as $key => $entry) {
            $converted = Money::$plugin->moneyService->getConversion(trim($entry->currency),'ZAR',$entry->price);
            $entry->priceSort = $converted;
            if (!Craft::$app->getElements()->saveElement($entry)) {
                echo "no conversion";
            }
            echo "{$entry->price} {$entry->currency}, converted: {$converted} == {$entry->priceSort}\n";
        }


        return $result;
    }

}
