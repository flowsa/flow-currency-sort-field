<?php
/**
 * @link https://craftcms.com/
 * @copyright Copyright (c) Pixel & Tonic, Inc.
 * @license MIT
 */

namespace flowsa\currencysortfield\controllers;

use Craft;
use craft\base\Element;
use craft\elements\Entry;
use flowsa\currencysortfield\Plugin;
use flowsa\currencysortfield\CurrencySortField;
use flowsa\money\Money;
use craft\helpers\DateTimeHelper;
use craft\helpers\Db;
use craft\models\Section;
use craft\web\Controller;
use craft\web\Request;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Guest Entries controller
 */
class SaveController extends Controller
{
    // Properties
    // =========================================================================

    /**
     * @inheritdoc
     */
    protected $allowAnonymous = true;

   
    public function actionIndex()
    {
        $entries = Entry::find()
            ->sectionId([15,17])
            ->all();

        foreach ($entries as $key => $entry) {
            $converted = Money::$plugin->moneyService->getConversion($entry->currency,'ZAR',$entry->price);
            $entry->priceSort = $converted;
            if (!Craft::$app->getElements()->saveElement($entry)) {
                echo "no conversion";
            }
            echo "{$entry->price} {$entry->currency}, converted: {$converted} == {$entry->priceSort}\n";
        }


        return true;
    }

}
