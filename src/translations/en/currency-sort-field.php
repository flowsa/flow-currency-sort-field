<?php
/**
 * Currency Sort field plugin for Craft CMS 3.x
 *
 * Normalise currencies
 *
 * @link      https://www.flowsa.com
 * @copyright Copyright (c) 2019 Flow Comunications
 */

/**
 * @author    Flow Comunications
 * @package   CurrencySortField
 * @since     1.0.0
 */
return [
    'Currency Sort field plugin loaded' => 'Currency Sort field plugin loaded',
];
